using Improbable.Gdk.Subscriptions;
using Pickups;
using UnityEngine;

namespace Fps
{
    [WorkerType(WorkerUtils.UnityClient)]
    public class MineClientVisibility : MonoBehaviour
    {
        [Require] private MineReader mineReader;

        private MeshRenderer cubeMeshRenderer;
        Collider myCollider;

        private void OnEnable()
        {
            if (!cubeMeshRenderer) { cubeMeshRenderer = GetComponentInChildren<MeshRenderer>(); }
            if (!myCollider) { myCollider = GetComponentInChildren<Collider>(true); }

            mineReader.OnUpdate += OnMineComponentUpdated;
            UpdateVisibility();
        }

        private void UpdateVisibility()
        {
            cubeMeshRenderer.enabled = mineReader.Data.IsActive;
            myCollider.enabled = mineReader.Data.IsActive;
        }

        private void OnMineComponentUpdated(Mine.Update update)
        {
            UpdateVisibility();
        }
    }
}
