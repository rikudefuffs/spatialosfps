using System.Collections;
using Improbable.Gdk.Core;
using Improbable.Gdk.Health;
using Improbable.Gdk.Subscriptions;
using Pickups;
using UnityEngine;

namespace Fps
{
    [WorkerType(WorkerUtils.UnityGameLogic)] //only the server worker will run this
    public class HealthPickupServerBehaviour : MonoBehaviour
    {
        /// <summary>
        /// This is a Writer object, which allows you to interact with and modify your SpatialOS components easily at runtime.
        /// In particular, this is a HealthPickupWriter, which allows you to access and write to the value of the HealthPickup
        /// component of the underlying linked entity. For more information about Readers, see the Writer API.
        /// 
        /// The [Require] annotation on the HealthPickupWriter is very important.
        /// This tells the GDK to inject this object when its requirements are fulfilled.
        /// A Writer’s requirements is that the underlying SpatialOS component is checked out on your worker-instance,
        /// and your worker-instance is authoritative over that component.
        /// </summary>
        [Require] HealthPickupWriter healthPickupWriter;
        [Require] HealthComponentCommandSender healthCommandRequestSender;

        Coroutine respawnCoroutine;
        Collider myCollider;

        void Start()
        {
            myCollider = GetComponentInChildren<Collider>(true);
        }

        void OnEnable()
        {
            // If the pickup is inactive on initial checkout - turn off collisions and start the respawning process.

            /*
             * Why would healthPickupWriter.Data.IsActive be false in OnEnable()?
             * 
             * In a large game, where there are multiple workers of each type (such as multiple UnityGameLogic workers),
             * the SpatialOS load-balancer decides how to divide write-access for components between the available workers.
             * 
             * The coroutine is local only to the worker that started it.
             *
             * If the worker loses write-access to the component then the script will become disabled for that entity
             * (and remember, we call StopCoroutine in OnDisable to cancel the coroutine).
             *
             * In theory, this leaves the entity in a state where InActive is false,
             * and no coroutine is running that will eventually change that.
             * 
             * Just as one worker lost write-access, another will be granted it by the load-balancer.
             * 
             * That newly authoritative worker now meets the criteria specified by the
             * [Require] syntax in the HealthPickupServerBehaviour class, and so the script will become enabled.
             * 
             * This is why in OnEnable() you should start the coroutine if healthPickupWriter.Data.IsActive is false.
             * 
             * The newly authoritative worker will not know how long the cool-down had already been running on the previous worker,
             * so the cool-down timer is ultimately “refreshed” at this point.
             * 
             * If this was a problem for our mechanic then we could store the timer’s progress in a new property,
             * but in this case we will keep it simple.
             */

            if (!healthPickupWriter.Data.IsActive)
            {
                respawnCoroutine = StartCoroutine(RespawnHealthPackRoutine());
            }
        }

        void OnDisable()
        {
            if (respawnCoroutine != null)
            {
                StopCoroutine(respawnCoroutine);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            // OnTriggerEnter is fired regardless of whether the MonoBehaviour is enabled/disabled.
            //For this reason, scripts which use OnTriggerEnter must check whether objects that are have annotations
            //[Require] are null (indicating that the requirements were not met) before using functions on those objects.
            if (healthPickupWriter == null) { return; }
            if (!other.CompareTag("Player")) { return; }

            HandleCollisionWithPlayer(other.gameObject);
        }

        void SetIsActive(bool isActive)
        {
            myCollider.enabled = isActive;
            healthPickupWriter?.SendUpdate //same as "if(!healthPickupWriter) {return;} "
            (
                new HealthPickup.Update
                {
                    IsActive = new Option<bool>(isActive)
                }
            );
        }

        void HandleCollisionWithPlayer(GameObject player)
        {
            LinkedEntityComponent playerSpatialOsComponent = player.GetComponent<LinkedEntityComponent>();
            if (playerSpatialOsComponent == null) { return; }
            HealthExposer healthExposer = player.GetComponent<HealthExposer>();
            if (!healthExposer || !healthExposer.CanBeHealed()) { return; }

            HealthModifier healthModifier = new HealthModifier();
            healthModifier.Amount = healthPickupWriter.Data.HealthValue;

            //-- This is a cross-worker interaction. --//
            //When you send a command it acts as a request, which SpatialOS delivers to the worker-instance
            //that has write-access for the component that the command is intended for.

            //Cross-worker interactions can be necessary when your game has multiple UnityGameLogic server-workers,
            //because the worker with write-access for the HealthPack entity may not be the same worker that has write-access
            //to the Player entity who has collided with that health pack.
            healthCommandRequestSender.SendModifyHealthCommand(playerSpatialOsComponent.EntityId, healthModifier);
            //----//

            // Toggle health pack to its "consumed" state
            SetIsActive(false);

            // Begin cool-down period before re-activating health pack
            respawnCoroutine = StartCoroutine(RespawnHealthPackRoutine());
        }

        private IEnumerator RespawnHealthPackRoutine()
        {
            yield return new WaitForSeconds(15f);
            SetIsActive(true);
        }
    }
}
