using Improbable.Gdk.Subscriptions;
using Pickups;
using UnityEngine;
namespace Fps
{
    [WorkerType(WorkerUtils.UnityClient)] //enables this behaviour only for this kind of worker (client)
    public class HealthPickupClientVisibility : MonoBehaviour
    {
        /// <summary>
        /// This is a Reader object, which allows you to interact with your SpatialOS components easily at runtime.
        /// In particular, this is a HealthPickupReader, which allows you to access the value of the HealthPickup component
        /// of the underlying linked entity. For more information about Readers, see the Reader API.
        ///
        /// The [Require] annotation on the HealthPickupReader is very important.
        /// This tells the GDK to inject this object when its requirements are fulfilled.
        /// A Reader’s requirements is that the underlying SpatialOS component is checked out on your worker-instance,
        /// regardless of authority.
        /// A Monobehaviour will only be enabled if all required objects have their requirements satisfied.
        /// </summary>
        [Require] HealthPickupReader healthPickupReader;
        MeshRenderer cubeMeshRenderer;
        Collider myCollider;


        void OnEnable()
        {
            if (!cubeMeshRenderer) { cubeMeshRenderer = GetComponentInChildren<MeshRenderer>(); }
            if (!myCollider) { myCollider = GetComponentInChildren<Collider>(true); }

            //The GDK automatically clears event handlers when a script is disabled,
            //therefore you do not need to manually remove the OnHealthPickupComponentUpdated callback.
            healthPickupReader.OnUpdate += OnHealthPickupComponentUpdated;

            UpdateVisibility();
        }

        void UpdateVisibility()
        {
            cubeMeshRenderer.enabled = healthPickupReader.Data.IsActive;
            myCollider.enabled = healthPickupReader.Data.IsActive;
        }

        void OnHealthPickupComponentUpdated(HealthPickup.Update update)
        {
            UpdateVisibility();
        }
    }
}
