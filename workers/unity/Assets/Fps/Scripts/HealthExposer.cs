using System.Collections;
using Improbable.Gdk.Core;
using Improbable.Gdk.Health;
using Improbable.Gdk.Subscriptions;
using Pickups;
using UnityEngine;

namespace Fps
{
    /// <summary>
    /// Exposes the health of the player so other scripts can access it
    /// </summary>
    [WorkerType(WorkerUtils.UnityGameLogic)]
    public class HealthExposer : MonoBehaviour
    {
        [Require] HealthComponentReader healthComponentReader;

        public bool CanBeHealed()
        {
            HealthComponent.Component healthData = healthComponentReader.Data;
            return healthData.Health < healthData.MaxHealth;
        }
    }
}
