using System.Collections;
using Improbable.Gdk.Core;
using Improbable.Gdk.Health;
using Improbable.Gdk.Subscriptions;
using Pickups;
using UnityEngine;

namespace Fps
{
    [WorkerType(WorkerUtils.UnityGameLogic)] //only the server worker will run this
    public class MineServerBehaviour : MonoBehaviour
    {
        [Require] MineWriter mineWriter;
        [Require] HealthComponentCommandSender healthCommandRequestSender;

        Collider myCollider;

        void Start()
        {
            myCollider = GetComponentInChildren<Collider>(true);
        }
        private void OnTriggerEnter(Collider other)
        {
            // OnTriggerEnter is fired regardless of whether the MonoBehaviour is enabled/disabled.
            //For this reason, scripts which use OnTriggerEnter must check whether objects that are have annotations
            //[Require] are null (indicating that the requirements were not met) before using functions on those objects.
            if (mineWriter == null) { return; }
            if (!other.CompareTag("Player")) { return; }

            HandleCollisionWithPlayer(other.gameObject);
        }

        void HandleCollisionWithPlayer(GameObject player)
        {
            LinkedEntityComponent playerSpatialOsComponent = player.GetComponent<LinkedEntityComponent>();
            if (playerSpatialOsComponent == null) { return; }

            HealthModifier healthModifier = new HealthModifier();
            healthModifier.Amount = mineWriter.Data.DamageValue * -1;

            //-- This is a cross-worker interaction. --//
            //When you send a command it acts as a request, which SpatialOS delivers to the worker-instance
            //that has write-access for the component that the command is intended for.

            //Cross-worker interactions can be necessary when your game has multiple UnityGameLogic server-workers,
            //because the worker with write-access for the HealthPack entity may not be the same worker that has write-access
            //to the Player entity who has collided with that health pack.
            healthCommandRequestSender.SendModifyHealthCommand(playerSpatialOsComponent.EntityId, healthModifier);
            //----//

            // Toggle health pack to its "consumed" state
            SetIsActive(false);
        }

        void SetIsActive(bool isActive)
        {
            myCollider.enabled = isActive;
            mineWriter?.SendUpdate
            (
                new Mine.Update
                {
                    IsActive = new Option<bool>(isActive)
                }
            );
        }
    }
}
